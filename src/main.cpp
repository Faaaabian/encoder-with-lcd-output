#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <Encoder.h>

// defines for connected pins and constant values

#define RESET_W 12                   // Reset for angle encoder
#define RESET_H 11                   // Reset for height encoder
#define LONG_SHORT_SWITCH 4          // Jumper bridge for switching between long and short mode
#define ENCODER1_ONE 3               // first encoder, first pin
#define ENCODER1_TWO 8               // first encoder, second pin
#define ENCODER2_ONE 2               // second encoder, first pin
#define ENCODER2_TWO 7               // second encoder, second pin
#define ENCODER1_PULSES_PER_REV 2400 // if you turn the encoder one revolution, what number does it show?
#define ENCODER2_PULSES_PER_REV 2400

// defining and initializing variables

int32_t encoderOneValue = 0;
int32_t encoderTwoValue = 0;
bool shortValues = false;

int encoderOnePreviousValue = 0;
int encoderTwoPreviousValue = 0;
bool shortValuesPrevious = false;

String toPrintFirstLine = "";
String toPrintSecondLine = "";

// Creating objects for libraries to communicate with encoders and display

Encoder encoder1(ENCODER1_ONE, ENCODER1_TWO);
Encoder encoder2(ENCODER2_ONE, ENCODER2_TWO);

LiquidCrystal_I2C lcd(0x27, 20, 4);

// function for modifying the String to be printed to the LCD-Display

void lcdOutput(int32_t value1, int32_t value2, bool isOutputShort)
{
  if (isOutputShort)
  {
    // short version
    toPrintFirstLine = "W: " + String(value1 * 360 / ENCODER1_PULSES_PER_REV) + (char)223;
    toPrintSecondLine = "H: " + String(value2) + "mm";
  }
  else
  {
    // long version
    toPrintFirstLine = "Winkel:  " + String(value1 * 360 / ENCODER1_PULSES_PER_REV) + (char)223;
    toPrintSecondLine = "Hoehe:   " + String(value2) + "mm";
  }
  // actual printing to the display
  lcd.clear();
  lcd.home();
  lcd.print(toPrintFirstLine);
  lcd.setCursor(0, 1);
  lcd.print(toPrintSecondLine);
}

// setup is the first to run at a boot - it runs just once

void setup()
{
  Serial.begin(9600); // for debugging

  pinMode(RESET_H, INPUT_PULLUP);
  pinMode(RESET_W, INPUT_PULLUP);
  pinMode(LONG_SHORT_SWITCH, INPUT_PULLUP);

  shortValues = digitalRead(LONG_SHORT_SWITCH);

  // initializin display
  lcd.init();
  lcd.backlight();
  lcdOutput(0, 0, shortValues);
}

void loop()
{
  // reading both encoders and probing the bridge for long/short values
  encoderOneValue = encoder1.read();
  encoderTwoValue = encoder2.read();
  shortValues = digitalRead(LONG_SHORT_SWITCH);

  // reading if one of the reset pins has connected
  if (!digitalRead(RESET_W))
  {
    encoder1.write(0);
    encoderOneValue = 0;
  }

  if (!digitalRead(RESET_H))
  {
    encoder2.write(0);
    encoderTwoValue = 0;
  }

  // if there is no change in any value, there is no need to update the display
  if (encoderOneValue != encoderOnePreviousValue || encoderTwoValue != encoderTwoPreviousValue || shortValues != shortValuesPrevious)
  {
    lcdOutput(encoderOneValue, encoderTwoValue, shortValues);
  }

  // setting the current values as previous ones for comparison in the next loop
  encoderOnePreviousValue = encoderOneValue;
  encoderTwoPreviousValue = encoderTwoValue;
  shortValuesPrevious = shortValues;

  Serial.println(encoderOneValue); // also for debugging
}